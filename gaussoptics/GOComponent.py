import numpy as np

'''Notes to future self:
    - Any component with length must also define an index of refraction
'''

class GOComponent:
    def __init__(self, transfer_matrix, length, location=None, plane=None):
        self.transfer_matrix = transfer_matrix
        if (plane is None):
            self.transfer_matrix_sagittal = transfer_matrix
            self.transfer_matrix_tangential = transfer_matrix
        self.length = length
        self.location = location

class Drift(GOComponent):
    def __init__(self, l, n=1, location=None):
        self.n = n
        super().__init__(self.transfer_function(l, self.n), l,
            location=location)

    def transfer_function(self, l, n=1):
        return np.array(([1, l/n], [0, 1]))

class ThinLens(GOComponent):
    def __init__(self, f, location=None):
        super().__init__(self.transfer_function(f), 0.0, location=location)
        
    def transfer_function(self, f):
        return np.array(([1, 0], [-1/f, 1]))

class Mirror(GOComponent):
    def __init__(self, angle=None, deg=False, f=None, radius=None, plane=None,
                 location=None):
        if angle is not None:
            if angle == 0:
                self.angle = None
            elif deg:
                self.angle = np.deg2rad(angle)
            else:
                self.angle = angle

        if radius is not None:
            if f is not None and radius != 2 * f:
                raise ValueError("either focal length or radius should be " +
                    "provided, but not both")
                return
            f = radius / 2

        # flat mirror
        if f is None or f == np.inf:
            super().__init__(np.identity(2), 0, location=location)
            return

        # paraxial mirror
        if self.angle is None:
            super().__init__(ThinLens.transfer_function(None, f), 0.0,
                location=location)
        # off-axis mirror
        else:
            if plane is None:
                raise ValueError("angle is specified but off-axis mirror " +
                    "plane not given")
                return
            super().__init__(None, 0.0, location=location)
            # overwrite default sagittal and tangential transfer functions
            self.transfer_matrix_sagittal = self.transfer_function(
                self.angle, f, 's')
            self.transfer_matrix_tangential = self.transfer_function(
                self.angle, f, 't')

    def transfer_function(self, a, f, p):
        if p is not None:
            if (p == 's'):
                f_adjusted = f / np.cos(np.deg2rad(a))
            elif (p == 't'):
                f_adjusted = f * np.cos(np.deg2rad(a))
            else:
                raise ValueError("invalid plane given (choose s or p)")
        else:
            f_adjusted = f
        return(ThinLens.transfer_function(None, f_adjusted))

class Plate(GOComponent):
    def __init__(self, n=1, n_incident=1, path_length=None, thickness=None,
        angle=None, deg=False, brewster=False, location=None):
        '''path_length is optical path length, not length of element in
        beamline. Either path_length or thickness may be specified, not both.
        Angle is incident angle. If brewster is True, angle may not be
        specified. n_incident should probably always be 1, but maybe your
        beamline is underwater?'''
        if brewster and angle is not None:
            raise ValueError("brewster is true and angle is specified")
            return
        if not brewster and angle is None:
            raise ValueError("angle not specified")
            return
        if path_length is None and thickness is None:
            raise ValueError("path_length or thickness must be provided")
            return
        if path_length is not None and thickness is not None:
            raise ValueError("path_length and thickness cannot be specified "
                "simultaneously")
            return

        if abs(n) < 1 or abs(n_incident) < 1:
            raise ValueError("Wow, that's some exotic material! I don't know "
                "how to handle fractional indices of refraction.")
            return
        # TODO: I'd have to dig into the geometry of negative refractive index
        # materials, so for now don't allow that
        if n < 0 or n_incident < 0:
            raise ValueError("can't handle negative refractive index "
                "materials")
            return

        self.n = n
        self.n_incident = n_incident

        # TODO: I'd have to dig into the geometry for non-Brewster's angles,
        # so for now don't allow arbitrary angles
        if angle is not None:
            raise ValueError("arbitrary angles not implemented")
            return

        # determine incident angle
        if brewster:
            self.angle = np.arctan(self.n / self.n_incident)
        elif degree:
            self.angle = np.deg2rad(angle)
        else:
            self.angle = angle

        # sanity check
        # it might be possible that negative angles make sense, but I don't
        # think it's worth the time to sort through
        if self.angle > np.pi / 2 or self.angle < 0:
            raise ValueError("your angle doesn't make sense!")

        # determine refraction angle
        sin_ref = self.n_incident / self.n * np.sin(self.angle)
        if sin_ref > 1:
            self.total_internal = True
            raise RuntimeError("total internal reflection")
            return
            # TODO if we are total_internal, then what?
        else:
            self.total_internal = False
            refract_angle = np.arcsin(sin_ref)

        # determine thickness
        if thickness is not None:
            self.thickness = thickness
            self.path_length = thickness / np.cos(refract_angle)
        else:
            self.path_length = path_length
            self.thickness = path_length * np.cos(refract_angle)

        # determine beamline length
        self.beamline_length = self.path_length * np.cos(
            self.angle - refract_angle)

        # finish initialization
        super().__init__(None, self.path_length, location=location)
        # overwrite default sagittal and tangential transfer functions
        self.transfer_matrix_sagittal = self.transfer_function(
            self.n / self.n_incident, self.thickness, 's')
        self.transfer_matrix_tangential = self.transfer_function(
            self.n / self.n_incident, self.thickness, 't')

    def transfer_function(self, n, thickness, plane):
        if plane == 's':
            l_adjusted = thickness * np.sqrt(n**2 + 1) / n**2
        elif plane == 't':
            l_adjusted = thickness * np.sqrt(n**2 + 1) / n**4
        else:
            raise ValueError("invalid plane given (choose s or t)")

        return Drift.transfer_function(None, l_adjusted)

class Interface():
    def __init__(self, n1=1, n2=1, radius=np.inf, angle=0, deg=False,
        location=None):
        self.n1 = n1
        self.n2 = n2
        self.radius = radius
        if deg:
            self.angle = np.deg2rad(angle)
        else:
            self.angle = angle
        self.location = location
        self.length = 0

        sin_ref = self.n1 / self.n2 * np.sin(self.angle)
        if sin_ref > 1:
            self.total_internal = True
            # TODO: if we are total_internal, then what?
            raise RuntimeError("total internal reflection")
            return
        else:
            self.total_internal = False
            refract_angle = np.arcsin(sin_ref)

        self.transfer_matrix_sagittal = self.transfer_function(
            self.n1, self.n2, self.angle, refract_angle, self.radius, 's')
        self.transfer_matrix_tangential = self.transfer_function(
            self.n1, self.n2, self.angle, refract_angle, self.radius, 't')

    def transfer_function(self, n1, n2, theta1, theta2, radius, plane):
        delta_ne_r = (n2 * np.cos(theta2) - n1 * np.cos(theta1)) / radius
        if plane == 's':
            return np.array(([1, 0], [delta_ne_r, 1]))
        elif plane == 't':
            cos_th = np.cos(theta2) / np.cos(theta1)
            return np.array(([cos_th, 0], [delta_ne_r, 1 / cos_th]))
        else:
            raise ValueError("invalid plane given (choose s or t)")

class BRF(Plate):
    pass

