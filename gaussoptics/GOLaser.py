import numpy as np

from .GOBeam import GaussBeam

class GaussLaser(GaussBeam):
	def __init__(self, wavelength, power,
				 radius=None, z_radius=0.0, divergence=None, m_squared=1,
				 waist=None, z_waist=0.0, q_waist=None,
				 polarization=None, polarization_deg=None,
				 polarization_axis=None):
		'''TODO:
			Handle divergence, m_squared, waist, z_waist, q_waist'''

		self.power = power

		super().__init__(wavelength, waist=radius, waist_loc=z_radius,
			polarization=polarization, polarization_deg=polarization_deg,
			polarization_axis=polarization_axis)

	def intensity(self, r, z):
		w = self.get_w_at_z(z)
		return 2 * self.power / (np.pi * w**2) * np.exp(-2 * r**2 / w**2)

	def max_w_at_intensity(self, threshold):
		return 2 * np.sqrt(self.power / (2 * np.e * np.pi * threshold))

class GainMedium:
	def __init__(self, n_total, wavelength_pump, length, residence_time,
				 absorbtion_pump, emission_signal):
		''' it is up to the user to give the proper length that accounts for
		possible round-trip passes through the gain medium'''
		self.n_total = n_total
		self.wavelength_pump = wavelength_pump
		self.length = length
		self.residence_time = residence_time
		self.absorbtion_pump = absorbtion_pump
		self.emission_signal = emission_signal

	def gain_threshold(self, cavity_loss):
		if cavity_loss > 1 or cavity_loss < 0:
			raise ValueError('Bad value for cavity_loss')
			return
		transmission = 1 - cavity_loss
		return -1 / self.length * np.log(transmission)

	def intensity_threshold(self, cavity_loss):
		try:
			g_th = self.gain_threshold(cavity_loss)
		except ValueError:
			raise

		h = 6.626e-34
		c = 2.998e8
		v = c / self.wavelength_pump
		return g_th * h * v / (self.absorbtion_pump * self.emission_signal
			* self.n_total * self.residence_time)

