import numpy as np

from .GOComponent import Drift

class Beamline:
    def __init__(self, beamline_container, start=0.0):
        self.start = start
        self.beamline_container = []
        self.assemble_beamline(beamline_container)
        
    def assemble_beamline(self, beamline_container):
        cur_loc = self.start
        for component in beamline_container:
            # Beamline assembly can automatically add drifts if locations are
            # specified
            if component.location is not None:
                if component.location < cur_loc:
                    raise RuntimeError('Nonsensical component location. ' +
                        'Components must be in order in beamline container. '
                        + 'Did you reuse a component in the beamline?')
                # insert drift component to account for missing space
                d = Drift(component.location - cur_loc, location=cur_loc)
                self.beamline_container.append(d)
                cur_loc = component.location
            else:
                component.location = cur_loc
            
            # Add component to beamline
            self.beamline_container.append(component)
            cur_loc += component.length
        
        self.end = cur_loc
        self.length = cur_loc - self.start

    def compute_transfer_matrix(self, z1=None, z2=None, start_component=None,
                                end_component=None, plane=None):
        '''start_component and end_component can be used to limit calculation
        to particular components; z1 and z2 can be specified to calculate the
        transfer matrix between arbitrary points in the beamline or to extend
        the calculation beyond the beamline endpoints'''
        if start_component is None:
            start_component = 0
        elif start_component < 0:
            raise ValueError('start_component cannot be less than 0.')
        elif start_component > len(self.beamline_container) - 1:
            raise ValueError('start_component cannot be greater than the ' +
                             'length of the beamline container minus 1.')

        if end_component is None:
            end_component = len(self.beamline_container)
        elif end_component < 0:
            raise ValueError('end_component cannot be less than 0.')
        elif end_component < start_component:
            raise ValueError('end_component cannot be less than ' +
                             'start_component.')
        elif end_component > len(self.beamline_container) - 1:
            raise ValueError('end_component cannot be greater than the ' +
                             'length of the beamline container minus 1.')

        # if either z1 or z2 is given, both should be defined
        if z1 is not None and z2 is None:
            z2 = self.end
        elif z2 is not None and z1 is None:
            z1 = self.start

        # propagate in reverse if z1 > z2
        if z1 is not None and z2 is not None and z1 > z2:
            ztmp = z1
            z1 = z2
            z2 = ztmp
            reverse = True
        else:
            reverse = False

        # start with an identity matrix
        mat = np.identity(2)

        # if z1 comes before the start of the beamline, add a drift component
        if z1 is not None and z1 < self.start:
            # check that z2 doesn't also come before the start of the beamline
            if z2 < self.start:
                return self._mat_mult(Drift(z2 - z1).transfer_matrix, mat,
                    reverse)
            d = Drift(self.start - z1)
            mat = self._mat_mult(d.transfer_matrix, mat, reverse)

        for component in (
                self.beamline_container[start_component:end_component]):
            # ignore components before z1
            if z1 is not None and component.location < z1:
                # if z1 is in the middle of the component, add a drift
                comp_end = component.location + component.length
                if z1 < comp_end:
                    # check that z2 doesn't also come in the middle
                    if z2 < comp_end:
                        return self._mat_mult(Drift(
                            z2 - z1, n=component.n).transfer_matrix, mat,
                            reverse)
                    d = Drift(component.location + component.length - z1,
                        n=component.n)
                    mat = self._mat_mult(d.transfer_matrix, mat, reverse)
                continue

            # add drift if z2 comes before the end of the component
            if z2 is not None and z2 < component.location + component.length:
                d = Drift(z2 - component.location, n=component.n)
                mat = self._mat_mult(d.transfer_matrix, mat, reverse)
                break

            # add the element, based on what plane is being calculated
            if plane is None:
                if component.transfer_matrix is None:
                    raise RuntimeError("A component in the beamline has " +
                        "plane dependence. Please specify which plane, " +
                        "sagittal ('s') or tangential ('t').")
                mat = self._mat_mult(component.transfer_matrix, mat, reverse)
            elif plane == 's':
                mat = self._mat_mult(component.transfer_matrix_sagittal, mat,
                    reverse)
            elif plane == 't':
                mat = self._mat_mult(component.transfer_matrix_tangential,
                    mat, reverse)
            else:
                raise ValueError("invalid plane " + plane + "given. Valid " +
                    "values are 's' and 't'")

        # if z2 comes after the end of the beamline, add a drift component
        if z2 is not None and z2 > self.end:
            # check that z1 doesn't also come after the end of the beamline
            if z1 > self.end:
                return self._mat_mult(Drift(z2 - z1).transfer_matrix, mat,
                    reverse)
            d = Drift(z2 - self.end)
            mat = self._mat_mult(d.transfer_matrix, mat, reverse)

        return mat

    def _mat_mult(self, mat_new, mat_old, invert=False):
        # Forward: C @ B @ A @ qi = qf
        # Reverse: qi = A^-1 @ B^-1 @ C^-1 qf
        if invert:
            return mat_old @ np.linalg.inv(mat_new)
        else:
            return mat_new @ mat_old


