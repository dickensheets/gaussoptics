import numpy as np

from .GOComponent import Drift

## Improvements (breaking changes)
#  - qi -> q_waist
#  - waist_loc -> z_waist
## (non-breaking changes)
#  - add polarization

class GaussBeam:
    def __init__(self, wavelength, qi=None, waist=None, waist_loc=0.0,
                 polarization=None, polarization_deg=None,
                 polarization_axis=None):
        ''' If polarization_axis is provided, polarization and 
        polarization_deg are ignored. polarization_deg takes precedent over
        polarization.'''
        self.wavelength = wavelength
        self.qi = qi
        self.waist = waist
        self.waist_loc = waist_loc
        if qi is None and waist is None:
            raise ValueError('Either qi or waist must be specified.')
            return
        if qi is not None and waist is not None:
            raise ValueError('qi and waist cannot both be specified.')
            return

        if polarization_axis is not None:
            if (polarization_axis == 'x'
                or polarization_axis == 'h'
                or polarization_axis == 'horizontal'):
                self.polarization = 0
            elif (polarization_axis == 'y'
                or polarization_axis == 'v'
                or polarization_axis == 'vertical'):
                self.polarization = np.pi / 2
            else:
                raise ValueError("polarization_axis should be one of: 'x', " +
                                 "'y', 'h', 'v', 'horizontal', or 'vertical'")
                return
        elif polarization_deg is not None:
            self.polarization = int(np.deg2rad(polarization_deg))
        else:
            self.polarization = polarization

        # calculate qi
        if qi is None:
            self.qi = self.w0_to_q0(waist, wavelength)
        # calculate waist
        else:
            self.waist = self.q0_to_w0(qi, wavelength)

        self.z = None # array of z locations
        self.q = None # array of q values
        self.w = None # array of beam widths

    def propagate(self, beamline, start=None, end=None, plane=None,
                  dims=1000):
        '''Propagate beam through beamline and calculate arrays for q and w
        start defaults to beamline.start; end defaults to beamline.end
        dims controls number of data points in z, q, w arrays'''
        if start is None:
            self.start = beamline.start
        else:
            self.start = start
        if end is None:
            self.end = beamline.end
        else:
            self.end = end

        # Calculate q at start
        if self.waist_loc != self.start:
            q_start = self.q_prop(self.qi, beamline.compute_transfer_matrix(
                z1=self.waist_loc, z2=self.start))
        else:
            q_start = self.qi

        # compute z, q, and w
        z = self.z = np.linspace(self.start, self.end, dims)
        z1 = self.start
        self.q = np.asarray([self.q_prop(
            q_start, beamline.compute_transfer_matrix(
            z1=z1, z2=zCur, plane=plane)) for zCur in z])
        self.w = np.sqrt(-self.wavelength / (np.pi * np.imag(1 / self.q)))

    def q_prop(self, qo, m):
        a, b, c, d = m.flat
        return (a * qo + b) / (c * qo + d)

    def get_z(self):
        if self.z is None:
            raise RuntimeError('You must propagate beam through a beamline ' +
                'before you can get z locations. Run propagate() first.')
            return np.nan
        return self.z

    def get_q(self):
        if self.q is None:
            raise RuntimeError('You must propagate beam through a beamline ' +
                'before you can get q values. Run propagate() first.')
            return np.nan
        return self.q

    def get_w(self):
        if self.w is None:
            raise RuntimeError('You must propagate beam through a beamline ' +
                'before you can get waist sizes. Run propagate() first.')
            return np.nan
        return self.w

    def get_w_max(self):
        if self.w is None:
            raise RuntimeError('You must propagate beam through a beamline ' +
                'before you can get waist sizes. Run propagate() first.')
            return np.nan
        return max(self.w)

    def get_w_min(self):
        if self.w is None:
            raise RuntimeError('You must propagate beam through a beamline ' +
                'before you can get waist sizes. Run propagate() first.')
            return np.nan
        return min(self.w)

    def get_w_max_in_range(self, z1, z2):
        if self.w is None:
            raise RuntimeError('You must propagate beam through a beamline ' +
                'before you can get waist sizes. Run propagate() first.')
            return np.nan
        i = np.searchsorted(self.z, z1)
        f = np.searchsorted(self.z, z2)
        return max(self.w[i:f])

    def get_w_min_in_range(self, z1, z2):
        if self.w is None:
            raise RuntimeError('You must propagate beam through a beamline ' +
                'before you can get waist sizes. Run propagate() first.')
            return np.nan
        i = np.searchsorted(self.z, z1)
        f = np.searchsorted(self.z, z2)
        return min(self.w[i:f])

    def get_q_at_z(self, z):
        if self.z is None:
            raise RuntimeError('You must propagate beam through a beamline ' +
                'before you can get a q value. Run propagate() first.')
            return np.nan
        i = np.searchsorted(self.z, z)
        return self.q[i]
        
    def get_w_at_z(self, z):
        if self.z is None:
            raise RuntimeError('You must propagate beam through a beamline ' +
                'before you can get a waist size. Run propagate() first.')
            return np.nan
        i = np.searchsorted(self.z, z)
        return self.w[i]

    def w0_to_q0(self, w0, wavelength):
        return complex(0,1) * np.pi * (w0**2) / wavelength

    def q0_to_w0(self, q0, wavelength):
        return np.sqrt(q0 * complex(0,-1) * wavelength / np.pi)

    def z_rayleigh(self, w0, wavelength):
        return np.pi * w0**2 / wavelength
