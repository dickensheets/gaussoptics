import numpy as np

from .GOBeamline import *

class Resonator(Beamline):
    def complex_q_inv(self, plane=None):
        mat = self.compute_transfer_matrix(plane=plane)
        a, b, c, d = mat.flat
        # Siegman Lasers, pp. 820
        q_a_inv = (d - a)/(2 * b) - np.emath.sqrt(((a + d) / 2)**2 - 1) / b
        q_b_inv = (d - a)/(2 * b) + np.emath.sqrt(((a + d) / 2)**2 - 1) / b
        return (q_a_inv, q_b_inv)

    def half_trace(self, plane=None):
        mat = self.compute_transfer_matrix(plane=plane)
        return np.trace(mat) / 2

    def is_stable(self, plane=None):
        if (self.half_trace(plane=plane))**2 < 1:
            return True
        return False

    def radius(self, plane=None):
        if not self.is_stable(plane=plane):
            return np.nan
        mat = self.compute_transfer_matrix(plane=plane)
        a, b, c, d = mat.flat
        return 2 * b / (d - a)

    def spot_size(self, lam, plane=None):
        if not self.is_stable(plane=plane):
            return np.nan
        mat = self.compute_transfer_matrix(plane=plane)
        a, b, c, d = mat.flat
        m = self.half_trace(plane=plane)
        # Siegman Lasers, pp. 820
        omega_squared = abs(b) * lam / np.pi * np.sqrt(1 / (1 - m**2))
        return np.sqrt(omega_squared)

    def qi(self, plane=None):
        q_a_inv, q_b_inv = self.complex_q_inv(plane=plane)
        if np.imag(q_a_inv) < 0:
            return 1 / q_a_inv
        return 1 / q_b_inv