# gaussoptics

## Description
This repository is a basic python library for analyzing gaussian beam propagation through a beamline, including resonator analysis. I don't plan to update or maintain this repo except as I use it for my projects.

## Usage
Use as you would a typical python library.

## License
MIT

## Project status
Unmaintained